(If godot-cpp is not automatically downloaded upon cloning this repository, run 
`git submodule add https://github.com/GodotNativeTools/godot-cpp`)

INSTRUCTIONS:
- `git submodule update --init --recursive`
- `cd godot-cpp`
- `scons platform=<your platform> generate_bindings=yes`
- `cd ..`
- `make`

In order to test that the GDNative script library is working correclty, run `godot projects/Node.gd`. 
The resulting output should be a printout of three dictionaries, whose `position` entries are 5, 10, 
and 15. In order to trigger the segfault, run the file `test` that was created with `make`.
