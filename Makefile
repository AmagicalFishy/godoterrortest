#*************************************
# Makefile for Godot library: HexLib #
#*************************************
#********************
# VARS 
###
CXX = g++
CXXFLAGS = -Wall \
		   -Igodot-cpp/include \
		   -Igodot-cpp/include/core \
		   -Igodot-cpp/include/gen \
		   -Igodot-cpp/godot_headers
SRC = MapTest
SRCDIR = ./
OBJECTS = $(addsuffix .o, $(addprefix $(SRCDIR), $(SRC)))

#********************
# RULES
###
build: $(OBJECTS)
	$(CXX) -o project/libMapTest.so -shared -rdynamic -g $^ -Lgodot-cpp/bin -lgodot-cpp.linux.debug.64
	$(CXX) $(CXXFLAGS) -g test.cpp -o test -Lgodot-cpp/bin -lgodot-cpp.linux.debug.64

#********************
# Primary Files
###
.SECONDEXPANSION:
$(OBJECTS): $$(addsuffix .cpp, $$*)
	$(CXX) $(CXXFLAGS) -fPIC -o $@ -c $(addsuffix .cpp, $*) -g -O3

#********************
# Other
###
clean: 
	rm -f *.o
	rm -f project/libMapTest.so
