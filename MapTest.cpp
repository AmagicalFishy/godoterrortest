#include <array>

#include <Godot.hpp>
#include <Node.hpp>
#include <Dictionary.hpp>
#include <Vector3.hpp>

class Map : public godot::Node {
    GODOT_CLASS(Map, godot::Node);

    private: 
        std::array<int, 100> cells_;

    public: 
        Map() { };
        void _init() { } ;
        void set_cell(godot::Vector3 coord, int number) {
            cells_[coord.x] = number;
        };
        godot::Dictionary get_cell(godot::Vector3 coord) {
            godot::Dictionary something;
            something["position"] = cells_[coord.x];

            return something;
        }
        static void _register_methods() {
            register_method("set_cell", &Map::set_cell);
            register_method("get_cell", &Map::get_cell);
        }
};

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);
};

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    godot::Godot::gdnative_terminate(o);
};

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    godot::Godot::nativescript_init(handle);
    godot::register_tool_class<Map>();
};
