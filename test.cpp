#include <array>
#include <iostream>

#include <Godot.hpp>
#include <Dictionary.hpp>

#include "MapTest.cpp"

int main() { 
    Map map;
    map.set_cell(godot::Vector3(0, 0, 0), 5);
    map.get_cell(godot::Vector3(0, 0, 0));

    std::cout << "Success\n";
}
